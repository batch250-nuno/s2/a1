package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    //[SECTION] Java Collection
    // are single unit of objects
    // useful for manipulating relevant pieces of data that can be used in different situations, more commonly with loops.

    public static void main(String[] args) {

        //[SECTION] Array
        //In Java, arrays are containers of values of the same type given a predefined amount of values.
        // Java arrays are more rigid; Once the size and data type are defined, they can no longer be changed.

        //Array Declaration
        //Syntax:
        // dataType[] identifier = new dataType[numOfElements];
        // "[]" indicates that the data type should be able to hold multiple values.
        // "new" keyword is for the non-primitive data types to tell Java to create the said variable.
        // the values of the array is initialized either with 0 or null.

        int[] intArray = new int[5];

        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 99;
//        intArray[5] = 100; // out bounds error

        // This will return/print the memory address of the array.
//        System.out.println(intArray);

        // To print the intArray, we need to import the Arrays class and use the .toString() method to convert the array into string when we print it into the terminal.
        System.out.println(Arrays.toString(intArray));

        //Declaration with Initialization
        // Syntax:
        // dataType[] identifier = {elementA, elementB, elementC, ...};
        // the compiler automatically specifies the size by counting the number of elements in the array.

        String[] names = {"John", "Jane", "Joe"};
//        names[3] = "Joey";
        System.out.println(Arrays.toString(names));

        //Sample java array methods:
        //Sort
        Arrays.sort(intArray);
        System.out.println("Order of items after the sort method: " + Arrays.toString(intArray));

        //Multidimensional Arrays
        // A two-dimensional array can be described by two lengths nested within each other, like a matrix
        // first length is row, second length is column

        String[][] classroom = new String[3][3];

        // First Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        // Second Row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        // Third Row
        classroom[2][0] = "Mickey Mouse";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

//        System.out.println(Arrays.toString(classroom));
        // we use the deepToString() method in accessing values for multidimensional arrays.
        System.out.println(Arrays.deepToString(classroom));

        // In Java, the size of the array cannot be modified. If there is a need to add or remove elements, new arrays must be created.

        //[SECTION] ArrayLists
        // are resizable arrays, wherein elements can be added or removed whenever it is needed.

        //Syntax:
        //ArrayList<T> identifier = new ArrayList<T>();
        // "<T>" is used to specify that the list can only have one type of object in a collection.
        //ArrayList cannot hold primitive data types, "java wrapper class" provides a way to use these types of data as object.
        // in short, Object version of primitive data types with method.

        // Declaring an ArrayList
//        ArrayList<int> numbers = new ArrayList<int>(); // Type argument cannot be of primitive type.
        ArrayList<Integer> numbers = new ArrayList<Integer>(); // valid

        //Declaring an ArrayList then initializing
//        ArrayList<String> students = new ArrayList<String>();

        //Declaring an ArrayList with values
        ArrayList<String> students = new ArrayList<String>(Arrays.asList("Jane", "Mike"));

        //add elements
        //Syntax: arrayListName.add(element);
        students.add("John");
        students.add("Paul");
        System.out.println(students);

        //Access an element/s
        //arrayListName.get(index);

        System.out.println(students.get(0));

        //add element to array
        students.add(0, "Jane v2");
        //update element of array
        students.set(0, "Jane v3");
        //remove specific element in array
        students.remove(1);
        //remove all elemeny in array
        students.clear();
        //get array length
        System.out.println((students.size()));

        //Hashmaps
        HashMap<String, String> sampleHashmap = new HashMap<String, String>() {
            {
                put("Teacher", "John");
                put("Artist", "Jane");
            }
        };

        //add or update
        sampleHashmap.put("Student", "Brandon");
        sampleHashmap.put("Student", "Jane");
        sampleHashmap.put("Dreamer", "Alice");

        //Accessing element
        System.out.println(sampleHashmap.get("Student"));

        //update
        sampleHashmap.replace("Student", "Jane v2");

        //remove specific element and key
        sampleHashmap.remove("Dreamer");

        //remove all
        sampleHashmap.clear();

        System.out.println(sampleHashmap);








    }
}
