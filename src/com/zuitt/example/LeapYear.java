package com.zuitt.example;

import java.util.Scanner;
public class LeapYear {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Input year to be check if Leap year :");
        int inputYear = input.nextInt();

        if (inputYear % 4 == 0 && inputYear % 100 != 0 || inputYear % 400 == 0 ) {
            System.out.println(inputYear + " is a Leap Year!");
        } else {
            System.out.println(inputYear + " is a NOT Leap Year!");
        }
    }


}
