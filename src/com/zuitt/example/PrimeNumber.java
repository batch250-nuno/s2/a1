package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class PrimeNumber {
    public static void main(String[] args) {
        int[] numbers = new int[5];

        numbers[0] = 2;
        numbers[1] = 3;
        numbers[2] = 4;
        numbers[3] = 5;
        numbers[4] = 6;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 != 0 || numbers[i] == 2 )  {
                System.out.println(numbers[i] + " is a PRIME number");
            } else {
                System.out.println(numbers[i] + " is a COMPOSITE number");
            }
        }

// For Arraylist
        ArrayList<String> friends = new ArrayList<String>();
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Joey");
        System.out.println("My friends are : " + friends);

// For HashMap
        HashMap<String, Integer> inventory = new HashMap<String, Integer>();
        inventory.put("tootpaste", 15);
        inventory.put("tootbrush", 20);
        inventory.put("soap", 12);
        System.out.println("Our current inventory consists of : " + inventory);





    }

}
