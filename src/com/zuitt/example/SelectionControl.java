package com.zuitt.example;

import java.util.Scanner;

public class SelectionControl {
    public static void main(String[] args) {
        //Java Operators

        int num1 = 30;

        if (num1 % 10 == 0) {
            System.out.println(num1 + " is Divisible by 10");
        } else {
            System.out.println(num1 + " is NOT Divisible by 10");
        }

        //Short Circuiting

        int x = 15;
        int y = 0;

        if (y != 0 && x / y == 0) {
            System.out.println("Result : " + x / y);
        } else {
            System.out.println("This will only run because of Short Circuiting");
        }

        int num2 = 24;

        Boolean result = (num2 > 0) ? true : false;
        System.out.println(result);

        //Switch Case
        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Enter a Number :");
        int directionValue = numberScanner.nextInt();

        switch (directionValue) {
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Invalid Input");
                break;
        }
    }
}
